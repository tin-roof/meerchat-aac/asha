package asha

import "errors"

const (
	// cookie elements
	cookieButton = "//*[@id=\"Body1\"]/div[1]/div/a"

	// login elements
	loginElem         = "//*[@id=\"hypLogInOutLink\"]"
	emailInputElem    = "//*[@id=\"txtEmailLogin\"]"
	passwordInputElem = "//*[@id=\"txtPasswordLogin\"]"
	signInButton      = "//*[@id=\"cmdSignIn\"]"
)

var (
	// ErrInternalError is returned when the system is down
	ErrInternalError = errors.New("internal error")

	// ErrPageNotFound is returned when the page is not found
	ErrPageNotFound = errors.New("page not found")
)

type ASHA struct {
	selenium *Selenium
}

func NewASHA(driverPath string) (*ASHA, error) {
	s, err := newSelenium(driverPath)
	if err != nil {
		return nil, err
	}

	return &ASHA{selenium: s}, nil
}

// Close makes sure everything is shut down properly
func (a *ASHA) Close() {
	a.selenium.Close()
}
