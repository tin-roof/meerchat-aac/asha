package asha

import (
	"fmt"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

const (
	seleniumPort = 8080
)

type Selenium struct {
	service   *selenium.Service
	WebDriver selenium.WebDriver
}

// newSelenium starts a new selenium client
func newSelenium(driverPath string) (*Selenium, error) {
	// start selenium
	service, err := selenium.NewChromeDriverService(driverPath, seleniumPort)
	if err != nil {
		return nil, err
	}

	// connect to selenium
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// setup the chrome settings
	chrCaps := chrome.Capabilities{
		Args: []string{
			"--headless",
			"--disable-gpu",
			"--no-sandbox",
		},
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%v/wd/hub", seleniumPort))
	if err != nil {
		return nil, err
	}

	return &Selenium{
		service:   service,
		WebDriver: wd,
	}, nil
}

// Close the selenium client
func (s *Selenium) Close() {
	s.WebDriver.Quit()
	s.service.Stop()
}
