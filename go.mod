module gitlab.com/tin-roof/meerchat-aac/asha

go 1.19

require (
	github.com/tebeka/selenium v0.9.9
	gitlab.com/tin-roof/meerchat-aac/logger v0.0.5
)

require (
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/google/uuid v1.3.0 // indirect
)
