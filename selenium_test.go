package asha

import "testing"

func TestNewSelenium(t *testing.T) {
	s, err := newSelenium()
	if err != nil {
		t.Error(err)
	}
	s.Close()
}
