package asha

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/tebeka/selenium"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

const (
	// certification lookup elements
	lookupURL   = "https://apps.asha.org/eweb/ashadynamicpage.aspx?site=ashacms&webcode=ccchome"
	lookupTitle = "ASHA Certification Verification"

	// form elements
	verifyByNameLocationElem = "//*[@id=\"OTIResponsiveContentPane\"]/table/tbody/tr[2]/td/div[3]/div[2]/div[2]/label"
	lookupFNameElem          = "txtFirstName"
	lookupLNameElem          = "txtLastName"
	lookupCertNumElem        = "txtAccountNumber"
	lookupStateDropdown      = "//*[@id=\"ddlStates\"]"
	verifyCertButtonName     = "//*[@id=\"697f7894_af72_48fb_9d42_6dd17777b1de_btnSearchByNameLocation\"]"
	verifyCertButtonNumber   = "//*[@id=\"697f7894_af72_48fb_9d42_6dd17777b1de_btnSearchByAccountNumber\"]"

	// results
	lookupResultLink     = "//*[@id=\"3b83861e_e882_4f4d_a452_90ea68cf6111\"]/div[1]/div[1]/div/a"
	lookupName           = "//*[@id=\"ec97780e_444d_4814_ba62_2c56baa21598_lblStatus\"]/div/div[1]/div[1]"
	lookupAccountNumber  = "//*[@id=\"ec97780e_444d_4814_ba62_2c56baa21598_lblStatus\"]/div/div[1]/div[2]"
	lookupCertification  = "//*[@id=\"ec97780e_444d_4814_ba62_2c56baa21598_lblStatus\"]/div/div[1]/div[3]"
	lookupExpirationDate = "//*[@id=\"ec97780e_444d_4814_ba62_2c56baa21598_lblStatus\"]/div/div[2]/div[3]"

	// types
	expirationDateFormat = "01/02/2006"
)

var (
	// ErrCertificationNotFound is returned when a certification is not found
	ErrCertificationNotFound = errors.New("certification not found")

	// ErrMissingRequiredInformation is returned when not enough information is provided to lookup a certification
	ErrMissingRequiredInformation = errors.New("missing required information in order to lookup certification")
)

type Certification struct {
	Certification string
	ExpiryDate    time.Time
	FirstName     string
	LastName      string
	Number        int
	State         string
	Valid         bool
}

// NewCertification returns a new Certification
func NewCertification() *Certification {
	return &Certification{}
}

// Lookup looks up the certification by name or number
func (c *Certification) Lookup(ctx context.Context, client *ASHA) error {
	ctx, log := logger.New(ctx, "certification.Lookup", logger.WithField("certification", c))

	log.Info(ctx, "starting lookup")

	// lookup certificate by ID number
	if c.Number != 0 {
		if err := c.lookupByNumber(ctx, client); err != nil {
			return err
		}

		return nil
	}

	// lookup certificate by name and state
	if c.FirstName != "" && c.LastName != "" && c.State != "" {
		if err := c.lookupByName(ctx, client); err != nil {
			return err
		}

		return nil
	}

	return ErrMissingRequiredInformation
}

// IsValid returns true if the certification is not expired
func (c *Certification) IsValid() bool {
	return !c.ExpiryDate.Before(time.Now())
}

// -- Private Methods --

// lookupByNumber looks up the certification by therapists name and state
func (c *Certification) lookupByName(ctx context.Context, client *ASHA) error {
	ctx, log := logger.New(ctx, "certification.lookupByName", logger.WithField("certification", c))

	log.Info(ctx, "starting lookup by name")

	// open the URL
	if err := client.selenium.WebDriver.Get(lookupURL); err != nil {
		return err
	}

	// wait for page to render
	// time.Sleep(1 * time.Second)

	// verify we are on the right page
	pageTitle, err := client.selenium.WebDriver.Title()
	if err != nil {
		return err
	}

	// check we didn't 404 or other error
	if !strings.Contains(pageTitle, lookupTitle) {
		return ErrPageNotFound
	}

	// dismiss cookie banner
	btn, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, cookieButton)
	if err == nil {
		btn.Click()
		time.Sleep(500 * time.Millisecond)
	}

	time.Sleep(500 * time.Millisecond)

	// select the Name & Location search form
	verifyByNameLocation, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, verifyByNameLocationElem)
	if err != nil {
		return err
	}
	verifyByNameLocation.Click()

	// enter first & last name
	fNameField, err := client.selenium.WebDriver.FindElement(selenium.ByID, lookupFNameElem)
	if err != nil {
		return err
	}
	fNameField.SendKeys(c.FirstName)

	lNameField, err := client.selenium.WebDriver.FindElement(selenium.ByID, lookupLNameElem)
	if err != nil {
		return err
	}
	lNameField.SendKeys(c.LastName)

	// TODO: is there a select/active element focus without clicking?
	// click the dropdown and enter State
	stateDD, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupStateDropdown)
	if err != nil {
		return err
	}
	// stateDD.Click()
	stateDD.SendKeys(c.State)

	// submit the name form
	submitButton, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, verifyCertButtonName)
	if err != nil {
		return err
	}
	submitButton.Click()

	// Wait for page render
	time.Sleep(1 * time.Second)

	// click the results link
	userLink, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupResultLink)
	if err != nil {
		return ErrCertificationNotFound
	}
	userLink.Click()

	// time.Sleep(1 * time.Second)

	// get the account number
	if err := c.getAccountNumber(client); err != nil {
		return err
	}

	// get the expiration date
	if err := c.getExpirationDate(client); err != nil {
		return err
	}

	// check the validity
	c.Valid = c.IsValid()

	return nil
}

// lookupByNumber looks up the certification by certificate ID number
func (c *Certification) lookupByNumber(ctx context.Context, client *ASHA) error {
	ctx, log := logger.New(ctx, "certification.lookupByNumber", logger.WithField("certification", c))

	log.Info(ctx, "starting lookup by number")

	// open the URL
	if err := client.selenium.WebDriver.Get(lookupURL); err != nil {
		return err
	}

	// wait for page to render
	time.Sleep(1 * time.Second)

	// verify we are on the right page
	pageTitle, err := client.selenium.WebDriver.Title()
	if err != nil {
		return err
	}

	// check we didn't 404 or other error
	if !strings.Contains(pageTitle, lookupTitle) {
		return ErrPageNotFound
	}

	// wait for page to render
	time.Sleep(1 * time.Second)

	// dismiss cookie banner
	btn, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, cookieButton)
	if err == nil {
		btn.Click()
		time.Sleep(500 * time.Millisecond)
	}

	time.Sleep(500 * time.Millisecond)

	// enter cert number
	certField, err := client.selenium.WebDriver.FindElement(selenium.ByID, lookupCertNumElem)
	if err != nil {
		return err
	}
	certField.SendKeys(fmt.Sprintf("%v", c.Number))

	// submit the form
	submitButton, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, verifyCertButtonNumber)
	if err != nil {
		return err
	}
	submitButton.Click()

	// wait for page render
	time.Sleep(1 * time.Second)

	// ensure result found
	userLink, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupResultLink)
	if err != nil {
		fmt.Println(err)
		return ErrCertificationNotFound
	}
	userLink.Click()

	time.Sleep(2 * time.Second)

	// get the name
	if err := c.getName(client); err != nil {
		return err
	}

	// get the certification type
	if err := c.getCertificationType(client); err != nil {
		return err
	}

	// get the expiration date
	if err := c.getExpirationDate(client); err != nil {
		return err
	}

	// check the validity
	c.Valid = c.IsValid()

	return nil
}

// getAccountNumber gets the account number from the lookup result
func (c *Certification) getAccountNumber(client *ASHA) error {
	accountField, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupAccountNumber)
	if err != nil {
		return err
	}
	accountNumber, err := accountField.Text()
	if err != nil {
		return err
	}
	formatAccNum := strings.Split(accountNumber, "\n")
	c.Number, err = strconv.Atoi(strings.Trim(formatAccNum[1], "X"))
	if err != nil {
		return err
	}

	return nil
}

// getCertificationType gets the certification type from the lookup result
func (c *Certification) getCertificationType(client *ASHA) error {
	certTypeField, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupCertification)
	if err != nil {
		return err
	}
	certData, err := certTypeField.Text()
	if err != nil {
		return err
	}
	cert := strings.Split(certData, "\n")
	c.Certification = cert[1]

	return nil
}

// getExpirationDate gets the expiration date from the lookup result
func (c *Certification) getExpirationDate(client *ASHA) error {
	expirationField, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupExpirationDate)
	if err != nil {
		return err
	}
	expirationDate, err := expirationField.Text()
	if err != nil {
		return err
	}
	formatExpDate := strings.Split(expirationDate, "\n")
	c.ExpiryDate, err = time.Parse(expirationDateFormat, formatExpDate[1])
	if err != nil {
		return err
	}

	return nil
}

// getName gets the name from the lookup result
func (c *Certification) getName(client *ASHA) error {
	nameField, err := client.selenium.WebDriver.FindElement(selenium.ByXPATH, lookupName)
	if err != nil {
		return err
	}
	nameData, err := nameField.Text()
	if err != nil {
		return err
	}
	name := strings.Split(strings.Split(nameData, "\n")[1], " ")
	c.FirstName = name[0]
	c.LastName = name[len(name)-1]

	return nil
}
