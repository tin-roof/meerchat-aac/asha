package asha

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestNewCertification(t *testing.T) {
	c := NewCertification()
	if c == nil {
		t.Error("NewCertification returned nil")
	}
}

func TestCertification_Lookup(t *testing.T) {
	c := NewCertification()
	c.Number = 0 // @TODO: Fill this in with a valid number

	client, err := NewASHA()
	if err != nil {
		t.Error(err)
	}
	defer client.Close()

	if err := c.Lookup(context.Background(), client); err != nil {
		t.Error(err)
	}
}

func TestCertification_LookupByNumber(t *testing.T) {
	c := NewCertification()
	c.Number = 0 // @TODO: fill this in with a valid number

	client, err := NewASHA()
	if err != nil {
		t.Error(err)
	}
	defer client.Close()

	if err := c.lookupByNumber(context.Background(), client); err != nil {
		t.Error(err)
	}
}

func TestCertification_LookupByName(t *testing.T) {
	c := NewCertification()
	c.FirstName = "" // @TODO: fill this in with a valid first name
	c.LastName = ""  // @TODO: fill this with valid first name
	c.State = ""     // @TODO: fill this in with a valid state

	client, err := NewASHA()
	if err != nil {
		t.Error(err)
	}
	defer client.Close()

	if err := c.lookupByName(context.Background(), client); err != nil {
		t.Error(err)
	}

	fmt.Println(c)
}

func TestCertification_IsValid(t *testing.T) {
	c := NewCertification()
	c.ExpiryDate = time.Now().AddDate(0, 0, -1)
	if c.IsValid() {
		t.Error("certification should not be valid")
	}
}
